<?php

/**
 * Implementing hook_settings for the settings page
 */
function xmlrpctester_settings() {
	$form['xmlrpc_server_address'] = array(
    '#title' => t('Server address'),
		'#type' => 'textfield',
		'#height' => 1,
		'#default_value' => _xmlrpctester_server(),
		'#description' => t('The fully qualified address of the XMLRPC server.'),
	);
	return system_settings_form($form);
}

