if(Drupal.jsEnabled) {
	$(document).ready( function() {
			$('#edit-methods').change( function() {
				/*alert($('#edit-methods').val());*/
				getSignature($('#edit-methods').val());
			});
		});
	getSignature = function (m_name) {
	var xml = '<?xml version="1.0"?><methodCall>' +
		'<methodName>system.methodSignature</methodName>' +
		'<params>' +
		'<param><value><string>'+m_name+'</string></value></param>' +
		'</params></methodCall>';

		$.ajax({
      url: 'http://d/6/sentry/xmlrpc.php',
      //url: Drupal.settings.xmlrpctester.xmlrpc,
			type: "POST",
			dataType: "xml",
			data: xml,
			success: function(xmlData) {
			// this is an undefined xml
			},
			complete: function(data) {
			// on complete we get back the xml, but it has a type of string
				var xmlobject = (new DOMParser()).parseFromString(data.responseText, "text/xml");
				var i = 0;
				var arg = new Array;
				$(xmlobject).find("value").each( function () {
					if(i>0) { // the first hit is the top level <value> tag, we dont need it
						if(i == 1) { // this is the return type
							var return_type = $(this).text() ? $(this).text() : 'undefined';
						}
						else { // here come the arguments
							arg.push($(this).text());
						}
					}
					i++;
				});
				$.get( 
            'http://d/5/nunet/?q=xmlrpc_method_form', 
            Drupal.settings.xmlrpctester.formAJAX
						{ narg: arg.length },
						function(form) {
							$('#xmlrpctester-method-form').remove();
							$('#edit-arguments').after( form );
						}
					);
			},
		});
	};
}
