<?php

/**
 * Implementing hook_menu
 */
function xmlrpctester_menu() {
  $items = array();
  $items['admin/settings/xmlrpctester'] = array(
    'title' => 'XMLRPC Tester Settings',
    'access arguments' => array('administer xmlrpctester'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmlrpctester_settings'),
    'file' => 'xmlrpctester_admin.inc',
    'description' => t('Configure XMLRPC tester settings.'),
  );

  $items['xmlrpctester'] = array(
    'title' => 'XMLRPC Testing Facility',
    'description' => t('Developer tools for testing XML-RPC methods.'),
    'page callback' => 'xmlrpctester_page',
    'access arguments' => array('access devel information'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['xmlrpctester/methods'] = array(
    'title' => 'Method list',
    'page callback' => 'xmlrpctester_method_list',
    'access arguments' => array('access devel information'),
    //'type' => MENU_LOCAL_TASK,
  );

  $items['xmlrpctester/custom'] = array(
    'title' => 'Custom XML POST testing',
    'description' => t('Form for testing XMLRPC POST requests.'),
    'page callback' => 'xmlrpctester_page',
    'access arguments' => array('access devel information'),
    //'type' => MENU_LOCAL_TASK,
  );

  $items['xmlrpctester/callmethod'] = array(
    'title' => 'Call method',
    'page callback' => 'xmlrpctester_sender',
    'description' => t('Method caller page for easy methods calls'),
    'access arguments' => array('access devel information'),
  );

  $items['xmlrpc_method_form'] = array(
    'title' => 'xmlprctester method form',
    'page callback' => 'xmlrpctester_method_form',
    'access arguments' => array('access devel information'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function xmlrpctester_methods_form() {
	/* Lets build a select list of all XML-RPC methods available */
  if (module_exists('service')) {
    foreach (module_implements('service') as $module) {
      $services = module_invoke($module, 'service'); // services that one module provides
      foreach ($services as $s) { // go service by service
        $options[$module][$s['#method']] = $s['#method'];
      }
    }
  }
  $methods = module_invoke_all('xmlrpc');

  foreach ($methods as $method) {
    $options['XMLRPC'][$method[0]] = $method[0];
  }

	$form['method'] = array(
		'#type' => 'select',
		'#title' => t('Available methods'),
		'#description' => t('Select an available method to call.'),
		'#options' => $options,
	);

	$form['arguments'] = array(
		'#type' => 'textarea',
		'#title' => t('Arguments'),
		'#description' => t('Write one argument per line. Eg. arg 1 in line 1, hit enter, arg 2 in line 2 etc...'),
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Call it'),
	);
	return $form;
}

function xmlrpctester_methods_form_submit($form, $form_values) {
  $method = $form_values['values']['method'];
  $arguments = explode("\n", $form_values['values']['arguments']);
  if (1 == count($arguments)) {
    $arguments = $arguments[0];
  }
  $url = _xmlrpctester_server();
  $result = xmlrpc($url, $method, $arguments);
  if (isset($result['error'])) {
    drupal_set_message(t('Error: !error', array('!error' => $result['error'])), 'error');
  }
  else {
    drupal_set_message(t('Method successful.'));
    dsm($result);
  }
}

function xmlrpctester_sender() {
	$output = drupal_get_form('xmlrpctester_methods_form');
	drupal_add_js(drupal_get_path('module', 'xmlrpctester') . '/xmlrpctester.js');

	return $output;
}

function xmlrpctester_method_list() {
  // Modules that implement hook_xmlrpc().
  $methods = module_invoke_all('xmlrpc');
  foreach ($methods as $method) {
    $row = array();
    // shorthand notation
    // verbous notation
    $name = $method[0];
    $signature = $method[2];
    $sig_return = array_shift($signature);
    $sig_args = implode(', ', $signature);
    $description = $method[3];
    $row[] = array(
      $name,
      t('Return: %return_type, arguments: %arg_tupes', 
        array(
          '%return_type' => $sig_return,
          '%arg_types' => $sig_args,
        )),
      $description,
    );
    $rows[] = $row;
  }
  $header = array(
    t('Name'),
    t('Signature'),
    t('Description'),
  );
  $output = theme('table', $header, $rows);
  return $output;
}

/**
 * The form for the testing page
 */
function xmlrpctester_xmlsend_form() {
	$server = variable_get('xmlrpc_server_address', _xmlrpctester_default_server());

  $default_message = _xmlrpctester_default_message();

	$form = array();
	$form['xml_field'] = array(
		'#type' => 'textarea',
		'#rows' => 10,
		'#required' => TRUE,
		'#description' => t('Test XML message. This will be sent using the POST method to the defined XMLRPC server: %server_url', array('%server_url' => $server)),
		'#default_value' => $default_message,
	);
	$form['server_address'] = array(
		'#type' => 'hidden',
		'#value' => $server,
		);
	$form['test_submit'] = array (
	  '#value' => t('Submit XML'),
		'#type' => 'submit'
	);
	return $form;
}

/**
 * AJAX callback to render a form for the method call
 * $narg gives the number of arguments we need
 * a textfield for
 */
function xmlrpctester_method_form() {
	static $form;
	$narg = $_GET['narg'];
	if(isset($form)) { // meaning we have already built it
		return($form);
	}
	else { // first hit
		$form = array();
		$form['args'] = array(
			'#type' => 'fieldset',
			'#title' => t('Method arguments'),
			'#collapsible' => FALSE,
			);
		for($n = 0; $n< $narg; $n++) {
			$form['args']['argument_' . $n] = array(
				'#type' => 'textfield',
				'#title' => t('Argument' . $n),
			);
		}
		print drupal_get_form('xmlrpctester_method_form');
	}
}

/**
 * Assemble the XMLRPC testing page, with form that submits the XML
 * to the pre-set url, and a table listing the available services
 */
function xmlrpctester_page() {
	$output = drupal_get_form('xmlrpctester_xmlsend_form');
	foreach (module_implements('service') as $module) {
		$services = module_invoke($module, 'service'); // services that one module provides
		foreach ($services as $s) { // go service by service
			$rows[] = array( 
				array('data' => $module, 'class' => 'left'),
				array('data' =>	$s['#method'], 'class' => 'right'),
			);
		}
	}
	$header = array(t('Module'), t('Method'));
	$modulelist = theme('table', $header, $rows);
	$output .= t('<h2>Avialble services</h2>') . $modulelist;
	return $output;
}


function xmlrpctester_test_submit($form_id, $form_values) {
	/* Sample XML, we overwrite $xml later */
  $xml = _xmlrpctester_default_message();

	$xml = $form_values['xml_field'];
	$type = array("Content-Type" => "text/xml");
	$method = 'POST';
	$result = drupal_http_request($form_values['server_address'], $type, $method, $xml);

	if(isset($result->error))	{
		drupal_set_message($result->error. ' / error code: ' . $result->code);
	}

  require_once './includes/xmlrpc.inc';
  require_once './includes/xmlrpcs.inc';
	/* from xmlrpc.inc _xmlrpc() */
	drupal_set_message(t('HTTP return code ' . $result->code));

  $message = xmlrpc_message($result->data);
  // Now parse what we've got back
  if (!xmlrpc_message_parse($message)) {
    // XML error
		drupal_set_message(t('Parse error. Not well formed.'));
		return;
  }
  // Is the message a fault?
  if ($message->messagetype == 'fault') {
		drupal_set_message(t('This message is a fault. Error code ' . $message->fault_code . ', ' . $message->fault_string));
		/* Uncomment this if you want to see the returned XML */
		/*dpr($message);*/
		return;
  }
	if(is_array($message->params[0])) {
		drupal_set_message(t('Content available and loaded'));
	}
	else {
		drupal_set_message($message->params[0]);
	}
}


function _xmlrpctester_server() {
  $default = _xmlrpctester_default_server();
  return variable_get('xmlrpc_server_address', $default);
}

/**
 * The default server url is what the module sets by default
 */
function _xmlrpctester_default_server() {
  $index_path = $_SERVER['PHP_SELF'];
  $subdir = str_replace('index.php', '', $index_path);
  $url = 'http://'. $_SERVER['HTTP_HOST'] . $subdir;

	$xmlrpc_server = module_invoke('xmlrpc_server', 'server_info');
  if (isset($xmlrpc_server)) { // Services module implements?
    $server_path = 'services/' . $xmlrpc_server['#path'];
  }
  else {
    $server_path = $url . 'xmlrpc.php';
  }
  return $server_path;
}

function _xmlrpctester_default_message() {
  $xml = <<<EOD
<?xml version="1.0"?>
<methodCall>
<methodName>node.load</methodName>
<params>
<param><value><int>1</int></value></param>
<param><value><array><data>
</data></array></value></param>
</params></methodCall>';
EOD;
  return $xml;
}
